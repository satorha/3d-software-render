/*#include <SDL/SDL.h>
#include <iostream>
#include <cstdlib>
#include <vector>
#include <cmath>


using namespace std;

const int resolution_w = 1280;
const int resolution_h = 720;
SDL_Surface* __screen_surface = NULL;

void initScreen()
{
    SDL_Init(SDL_INIT_VIDEO);

    __screen_surface = SDL_SetVideoMode(resolution_w, resolution_h, 32, SDL_SWSURFACE);

    if(__screen_surface == nullptr)
    {
        cout << "Erro ao criar janela.";
        exit(0);
    }
}

void updateScreen()
{
    SDL_Flip(__screen_surface);
    SDL_FillRect(__screen_surface, nullptr, SDL_MapRGB(__screen_surface->format, 0, 0, 0));
}

void quit()
{
    SDL_Quit();
}



class matrix
{
public:
    float data[9];

    matrix()
    {

    }
    matrix(const vector<float> d)
    {
        for(int i=0; i < 9; i++)
            data[i] = d[i];
    }
    inline float& at(int l, int c)
    {
        return data[c + 3*l];
    }
    inline float at(int l, int c) const
    {
        return data[c + 3*l];
    }
    matrix multiply(matrix& other)
    {
        matrix new_matrix;

        for(int l=0; l < 3; l++)
            for(int c=0; c < 3; c++)
            {
                float cont=0;
                for(int k=0; k < 3; k++)
                {
                    cont += at(l, k) * other.at(k, c);
                }
                new_matrix.at(l, c) = cont;
            }
        return new_matrix;
    }
};



class vetor
{
public:
    float x;
    float y;
    float z;

    vetor neg() const
    {
        vetor teste;
        teste.x = -x; teste.y = -y; teste.z = -z;
        return teste;
    }

    void multiplica(float val)
    {
        x *= val;
        y *= val;
        z *= val;
    }
    void soma(const vetor& val)
    {
        x += val.x;
        y += val.y;
        z += val.z;
    }
    float escalar(const vetor& outro) const
    {
        return x*outro.x + y*outro.y + z*outro.z;
    }

    float modulo()
    {
        return sqrt(x*x + y*y + z*z);
    }

    void normaliza()
    {
        multiplica(1/modulo());
    }

    void transform(const matrix& m)
    {
        vetor old = *this;
        x = old.x * m.at(0, 0) + old.y * m.at(0, 1) + old.z * m.at(0, 2);
        y = old.x * m.at(1, 0) + old.y * m.at(1, 1) + old.z * m.at(1, 2);
        z = old.x * m.at(2, 0) + old.y * m.at(2, 1) + old.z * m.at(2, 2);
    }

};

const float distancia_focal = 1;
const float distancia_focal2 = distancia_focal*distancia_focal;
const vetor camera{0, 0, distancia_focal};


bool project(vetor v, float* out)
{
    if(v.z < distancia_focal)
        return false;

    float k = distancia_focal/v.z;

    v.multiplica(k);

    out[0] = v.x*resolution_w/2 + resolution_w/2;;
    out[1] = v.y*resolution_h/2 + resolution_h/2;;

    return true;
}

vetor project(vetor v)
{
    vetor retorno;
    float k = distancia_focal/v.z;

    v.multiplica(k);

    retorno.x = v.x*resolution_w/2 + resolution_w/2;;
    retorno.y = v.y*resolution_h/2 + resolution_h/2;;

    return retorno;
}

void drawPoint(vetor v)
{
    int meio_x = resolution_w/2;
    int meio_y = resolution_h/2;
    SDL_Rect area;
    area.w = 10;
    area.h = 10;
    area.x = meio_x + v.x - area.w/2;
    area.y = meio_y + v.y - area.h/2;
    SDL_FillRect(__screen_surface, &area, SDL_MapRGB(__screen_surface->format, 255, 255, 255));
}


void desenhaPontos(const vector<vetor>& lista)
{
    float coisas[lista.size()*2];

    for(int i=0; i < lista.size(); i++)
    {
        if(!project(lista[i], &coisas[2*i]))
            continue;
        drawPoint({coisas[2*i]*resolution_w/2, coisas[2*i+1]*resolution_h/2, 0});
    }
}

void fillScanline(SDL_Surface* out, int scanline, int start, int end, Uint32 Color)
{
    if(scanline < 0)
        scanline = 0;
    if(scanline >= out->h)
        return;

    start = max(start, 0);
    end  = min(end, out->w-1);
    Uint32 *pixels = (Uint32*) out->pixels;
    int offset = (out->pitch / sizeof(Uint32)) * scanline + start;
    Uint32* currentpixel = (pixels + offset);

    for( ; start <= end; start++)
    {
        *currentpixel = Color;
        currentpixel++;
    }
}

void line(SDL_Surface* out, int x0, int y0, int x1, int y1, Uint32 color)
{
  int dx = abs(x1-x0), sx = x0<x1 ? 1 : -1;
  int dy = abs(y1-y0), sy = y0<y1 ? 1 : -1;
  int err = (dx>dy ? dx : -dy)/2, e2;

  Uint32 *pixels = (Uint32*) out->pixels;

  for(;;){
    int offset = (out->pitch / sizeof(Uint32)) * y0 + x0;

    if(x0 >= 0 && y0 >= 0 && x0 < out->w && y0 < out->h)
        *(pixels + offset) = color;

    if (x0==x1 && y0==y1) break;
    e2 = err;
    if (e2 >-dx) { err -= dy; x0 += sx; }
    if (e2 < dy) { err += dx; y0 += sy; }
  }
}

bool fixLine(vetor* pontos)
{
    if(pontos[0].z >= 1 && pontos[1].z >= 1)
        return true;
    if(pontos[0].z < 1 && pontos[1].z < 1)
        return false;

    int nao_visivel = (pontos[0].z < 1) ? 0 : 1;
    int visivel = !nao_visivel;

    vetor vet = pontos[nao_visivel];
    vet.soma(pontos[visivel].neg());
    //vet.normaliza();
    //vet.multiplica((1 - pontos[visivel].z)/vet.z);
    //(pontos[nao_visivel] = pontos[visivel]).soma(vet);

    float k = (1 - pontos[visivel].z)/vet.z;


    pontos[nao_visivel].x = pontos[visivel].x + k*vet.x;
    pontos[nao_visivel].y = pontos[visivel].y + k*vet.y;
    pontos[nao_visivel].z = 1;


    return true;
}

void fillFlatTriangle(SDL_Surface* out, vetor v1, vetor v2, vetor v3, Uint32 Color)
{
    if(v2.x > v3.x)
        swap(v2, v3);

    const double dy1 = (v3.x - v1.x)/(v3.y-v1.y);
    const double dy2 = (v2.x - v1.x)/(v2.y-v1.y);

    double inicio_inicial = v2.x;
    double fim_inicial = v3.x;

    if(v1.y > v2.y)
    {
        inicio_inicial = v2.x;
        fim_inicial = v3.x;
    }
    else
    {
        inicio_inicial = v1.x;
        fim_inicial = v1.x;
    }


    int scanline_inicial = min(v2.y, v1.y);
    int scanline_final = max(v1.y, v2.y);

    if(scanline_inicial < 0)
    {
        inicio_inicial += dy2 * abs(scanline_inicial);
        fim_inicial += dy1 * abs(scanline_inicial);
        scanline_inicial = 0;
    }

    if(scanline_final > resolution_h)
        scanline_final = resolution_h;

    int altura = scanline_final - scanline_inicial;

    int desenhadas=0;
    for(int i = 0; i < altura; i++)
    {
        int x_inicial = inicio_inicial + dy2*i;
        int x_final = fim_inicial + dy1*i;
        fillScanline(__screen_surface, scanline_inicial+i, x_inicial, x_final, Color);
        desenhadas++;

    }

}

#include <algorithm>
void fillTriangle(SDL_Surface* out, const vetor* pontos, Uint32 color)
{
    vetor copia_pontos[3] = {pontos[0], pontos[1], pontos[2]};
    std::sort(std::begin(copia_pontos), std::end(copia_pontos), [](const vetor& a, const vetor& b){return a.y < b.y;});
    {
        vetor p4{ copia_pontos[0].x + ((copia_pontos[1].y - copia_pontos[0].y) /(copia_pontos[2].y - copia_pontos[0].y)) * (copia_pontos[2].x - copia_pontos[0].x),copia_pontos[1].y, 0};

        fillFlatTriangle(out, copia_pontos[0], copia_pontos[1], p4, color);
        fillFlatTriangle(out, copia_pontos[2], copia_pontos[1], p4, color);
    }

}

void desenhaTriangulo(const vetor* pontos, Uint32 cor)
{
    const pair<int, int> linhas_validas[3] = {{1, 2}, {2, 0}, {0, 1}};

    int pivot = 0;
    //seleciona um ponto na frente da camera
    for( ; pivot < 3; pivot++)
    {
        if(pontos[pivot].z > 1)
            break;
    }

    if(pivot >= 3)
        return; //triangulo nao visivel


    vetor linha[2] = {pontos[pivot], pontos[linhas_validas[pivot].first]};
    vetor linha2[2] = {pontos[pivot], pontos[linhas_validas[pivot].second]};
    fixLine(linha);
    fixLine(linha2);

    vetor triangulo_raster[3]  = {project(pontos[pivot]), project(linha[1]), project(linha2[1])};

    fillTriangle(__screen_surface, triangulo_raster, cor);
}


void desenhaQuad(vetor* pontos)
{
    Uint32 cor = SDL_MapRGBA(__screen_surface->format, 255, 0, 0, 255);
    Uint32 cor2 = SDL_MapRGBA(__screen_surface->format, 255, 255, 0, 255);

    vetor coisa[] = {pontos[0], pontos[1], pontos[2], pontos[3], pontos[0]};
    desenhaTriangulo(coisa, cor);
    desenhaTriangulo(&coisa[2], cor);

    float resultados[8];


        vetor linha[2] = {pontos[0], pontos[1]};
        if(!fixLine(linha))
            return;
        project(linha[0], &resultados[0]);
        project(linha[1], &resultados[2]);

        vetor linha2[2] = {pontos[2], pontos[3]};
        if(!fixLine(linha2))
            return;
        project(linha2[0], &resultados[4]);
        project(linha2[1], &resultados[6]);
     vetor pontos_finais[] = {{resultados[0], resultados[1]},
                              {resultados[2], resultados[3]},
                              {resultados[4], resultados[5]},
                              {resultados[6], resultados[7]},
                              {resultados[0], resultados[1]}};

     line(__screen_surface, pontos_finais[0].x, pontos_finais[0].y, pontos_finais[1].x, pontos_finais[1].y, cor2);
     line(__screen_surface, pontos_finais[2].x, pontos_finais[2].y, pontos_finais[3].x, pontos_finais[3].y, cor2);
     line(__screen_surface, pontos_finais[0].x, pontos_finais[0].y, pontos_finais[3].x, pontos_finais[3].y, cor2);
     line(__screen_surface, pontos_finais[1].x, pontos_finais[1].y, pontos_finais[2].x, pontos_finais[2].y, cor2);

}

int main(int argc, char *argv[])
{
    freopen("CON","w",stdout);

    initScreen();

    vector<vetor> pontos{{-100, -20, 80}, {100, -20, 80}, {100, 20, 80}, {-100, 20, 80},
                         {-100, -20, 40}, {100, -20, 40}, {100, 20, 40}, {-100, 20, 40}};

    const float ang = 0.03;

    matrix transformador({cos(ang), 0, -sin(ang),
                             0,     1,    0,
                         sin(ang), 0, cos(ang)});
    matrix transformador2({cos(-ang), 0, -sin(-ang),
                             0,     1,    0,
                         sin(-ang), 0, cos(-ang)});

    SDL_Event event;
    Uint8 *keystate = SDL_GetKeyState(NULL);
    while(1)
    {
        while(SDL_PollEvent(&event))
        {
            switch(event.type)
            {
                case SDL_QUIT:
                    goto SAIR;

                default:
                    break;
            }
        }

        if(keystate[SDLK_d])
        {
            for(vetor& p : pontos)
                p.transform(transformador);
        }
        else if(keystate[SDLK_a])
        {
            for(vetor& p : pontos)
                p.transform(transformador2);
        }
        else if(keystate[SDLK_w])
        {
            for(int i=0; i < pontos.size(); i++)
            {
                pontos[i].z -= 0.3;
            }
        }
        else if(keystate[SDLK_s])
        {
            for(int i=0; i < pontos.size(); i++)
            {
                pontos[i].z += 0.3;
            }
        }

        //desenhaPontos(pontos);
        desenhaQuad(&pontos[0]);
        //desenhaQuad(&pontos[4]);
        updateScreen();
        SDL_Delay(15);

    }
 SAIR:

    quit();
    system("pause");

    return 0;
}*/
